<?php
// Heading
$_['heading_title']    = 'Рассылка';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Ура! Вы подписаны на email-рассылку!';
$_['text_edit']        = 'Edit newsletters Module';

// Entry
$_['entry_status']     = 'Cnfnec';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Newsletters module!';

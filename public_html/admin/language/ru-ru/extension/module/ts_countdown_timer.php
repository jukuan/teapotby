<?php
// Heading
$_['heading_title']			= '<b>CountDown Timer <a href="https://tramplin-studio.store/" target="blank">by Tramplin Studio</a></b>';

// Buttons
$_['button_save']			= 'Сохранить';
$_['button_cancel']			= 'Отменить';
$_['button_enable_all']		= 'Влючить все';
$_['button_disable_all']	= 'Отключить все';

// Text
$_['text_module']			= 'Модули';
$_['text_extension']		= 'Расширения';
$_['text_edit']				= 'Изменение настроек модуля';
$_['text_success']			= 'Настройки модуля обновлены!';
$_['text_settings_title_1']	= 'Настройки отображения';
$_['text_settings_title_2']	= 'Другие настройки';
$_['text_customize_title_1']= 'Дизайн заголовка';
$_['text_customize_title_2']= 'Дизайн цифрового табло';
$_['text_timezone_default']	= '(По умолчанию)';
$_['text_timer_title']		= 'Акция закончится через:';

// Tabs
$_['tab_settings'][0]	    = 'Настройки';
$_['tab_settings']['product']	    = 'Товар';
$_['tab_settings']['category']	    = 'Категории';
$_['tab_settings']['manufacturer']	= 'Производители';
$_['tab_settings']['special']	    = 'Акции';
$_['tab_settings']['search']	    = 'Поиск';
$_['tab_settings']['latest']	    = 'Новинки (модуль)';
$_['tab_settings']['bestseller']	= 'Хиты продаж (модуль)';
$_['tab_settings']['specialmod']	= 'Акции (модуль)';
$_['tab_settings']['featured']	    = 'Рекомендуемые (модуль)';
$_['tab_settings']['related']	    = 'Рекомендуемые (стр. товара)';
$_['tab_customize']			= 'Оформление';
$_['tab_help']				= 'Помошь! <i class="fa fa-question-circle" style="color:#1e91cf;">&nbsp;</i>';

// Entry
$_['entry_status']				= 'Статус';
$_['entry_timezone']			= 'Часовой пояс';
$_['entry_title_timer']			= 'Отображать заголовок над таймером';
$_['entry_title_text_timer']	= 'Текст заголовка над таймером';
$_['entry_separator_timer']		= 'Отображать разделители';
$_['entry_separator_timer_help']= 'XX : XX : XX : XX';
$_['entry_caption_timer']		= 'Отображать текстовые обозначения';
$_['entry_caption_timer_help']	= 'XX дней XX часов XX минут XX секунд';
$_['entry_countdays_timer']		= 'Считать количество дней';
$_['entry_countdays_timer_help']= 'Если дни скрыты и эта опция выключена, то количество часов = кол-во дней * 24 + часы';
$_['entry_days_timer']			= 'Отображать количество дней';
$_['entry_sec_timer']			= 'Отображать количество секунд';

// Customize
$_['entry_customize_title_size']		= 'Размер шрифта заголовка';
$_['entry_customize_title_style']		= 'Стиль шрифта заголовка';
$_['entry_customize_title_color']		= 'Цвет шрифта заголовка';
$_['entry_customize_tablet_bgexists']	= 'Отображать фон цифровой ячейки';
$_['entry_customize_tablet_bgcolor']	= 'Цвет фона цифровой ячейки';
$_['entry_customize_tablet_bggrad']		= 'Градиент фона цифровой ячейки';
$_['entry_customize_tablet_brdexists']	= 'Отображать рамки у цифровой ячейки';
$_['entry_customize_tablet_brdcolor']	= 'Цвет рамки цифровой ячейки';
$_['entry_customize_tablet_brdradius']	= 'Радиус закругления углов цифровой ячейки (pixels)';
$_['entry_customize_tablet_size']		= 'Размер шрифта в цифровой ячейке';
$_['entry_customize_tablet_style']		= 'Стиль шрифта в цифровой ячейке';
$_['entry_customize_tablet_color']		= 'Цвет шрифта в цифровой ячейке';
$_['entry_customize_separator_color']	= 'Цвет разделителей между ячейками';
$_['entry_tstyle_bold']					= 'Жирный';
$_['entry_tstyle_italic']				= 'Курсив';
$_['entry_tstyle_underline']			= 'Подчеркивание';

// Error
$_['error_permission']		= 'У Вас нет прав для управления этим модулем!';

// Help
$_['text_help'] = '<p><b>CountDown Timer v1.1 <a href="https://tramplin-studio.store/" target="blank">by Tramplin Studio</a></b> - это очень гибкий в настройке модуль, который выводит таймер обратного отсчета для акционных товаров на всех возможных страницах и во всех стандартных модулях OpenCart. Широкие возможности настройки дизайна позволяют вписать таймер в любой шаблон.<br>
Таймер обратного отсчета акции позволит привлечь внимание посетителей интернет-магазина к акционным товаром, тем самым значительно повысить конверсию магазина.</p>

<p>Модуль выводит числовой таймер на следующих страницах и модулях:<br>
<ul>
	<li>cтраница "Товар" (product/product);</li>
	<li>cтраница "Категория" (product/category);</li>
	<li>cтраница "Производитель" (product/manufacturer);</li>
	<li>cтраница "Акции" (product/special);</li>
	<li>cтраница "Поиск" (product/search);</li>
	<li>модуль "Новинки" (module/latest);</li>
	<li>модуль "Хиты продаж" (module/bestseller);</li>
	<li>модуль "Акции" (module/special);</li>
	<li>модуль "Рекомендуемые" (module/featured);</li>
	<li>модуль "Рекомендуемые" (product/product) на странице товара.</li>
</ul>
</p>

<p>Для каждой страницы и модуля можно отдельно настроить ряд внешних параметров таймера:<br>
<ul>
	<li>отображать таймер на отдельной странице или в модуле;</li>
	<li>заголовок таймера: "Акция закончится через";</li>
	<li>текстовые разделители (XX : XX : XX : XX);</li>
	<li>текстовые обозначения величин (XX день, XX час, XX минута, XX секунда);</li>
	<li>количество дней (также скрывается и текстовое обозначение этой величины);</li>
	<li>количество дней в часах (Пример: 5 дней * 24 часа = 120 часов + остаток за текущий день. Примечание: работает только если "количество дней" скрыто);</li>
	<li>количество секунд (также скрывается и текстовое обозначение этой величины).</li>
</ul>
</p>

<p>Таймер для каждого отдельного товара включается при добавлении/редактировании товара во вкладке "Акция".<br>
Необходимо указать новую стоимость и дату окончания акции. Если не указать дату окончания, то числовой таймер не отобразится для этого товара.</p>

<p>Если вы хотите чтобы таймер какого либо товара отсчитывал время до конца дня, то установите дату окончания акции "N дней", отключите опцию "Отображать количество дней", а опцию "Считать количество дней" включите. Например, если акция заканчивается через 5 дней, то ежедневно на протяжении 5-и дней будет отображаться таймер отсчета до конца текущих суток.<br>
Такой маневр приводит к еще большему повышению конверсии в продажу!</p>';

$_['text_author']			= '<p>Данный модуль является завершенным продуктом. Дальнейшее обновление модуль маловероятно.<br>Смотрите другие модули нашей разработки на нашем сайте:</p>
								<p>Сайт: <a href="https://tramplin-studio.store/" target="_blank">https://tramplin-studio.store/</a><br>
								E-mail: <a href="mailto:info@tramplin-studio.store">info@tramplin-studio.store</a></p>';
?>
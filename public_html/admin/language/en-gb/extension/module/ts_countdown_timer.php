<?php
// Heading
$_['heading_title']			= '<b>CountDown Timer <a href="https://tramplin-studio.store/" target="blank">by Tramplin Studio</a></b>';

// Buttons
$_['button_save']			= 'Save';
$_['button_cancel']			= 'Сancel';
$_['button_enable_all']		= 'Enable All';
$_['button_disable_all']	= 'Disable All';

// Text
$_['text_module']			= 'Modules';
$_['text_extension']		= 'Extensions';
$_['text_edit']				= 'Edit module settings';
$_['text_success']			= 'Module\'s settings has been updated!';
$_['text_settings_title_1']	= 'Display settings';
$_['text_settings_title_2']	= 'Other settings';
$_['text_customize_title_1']= 'Header design';
$_['text_customize_title_2']= 'Design of numeric display';
$_['text_timezone_default']	= '(default)';
$_['text_timer_title']		= 'Sale ends in:';

// Tabs
$_['tab_settings'][0]	    = 'Settings';
$_['tab_settings']['product']	    = 'Product';
$_['tab_settings']['category']	    = 'Category';
$_['tab_settings']['manufacturer']	= 'Manufacturer';
$_['tab_settings']['special']	    = 'Special';
$_['tab_settings']['search']	    = 'Search';
$_['tab_settings']['latest']	    = 'Latest (module)';
$_['tab_settings']['bestseller']	= 'Bestseller (module)';
$_['tab_settings']['specialmod']	= 'Special (module)';
$_['tab_settings']['featured']	    = 'Featured (module)';
$_['tab_settings']['related']	    = 'Related (product page)';
$_['tab_customize']			= 'Customize';
$_['tab_help']				= 'Help! <i class="fa fa-question-circle" style="color:#1e91cf;">&nbsp;</i>';

// Entry
$_['entry_status']				= 'Status';
$_['entry_timezone']			= 'Timezone';
$_['entry_title_timer']			= 'Header exists';
$_['entry_title_text_timer']	= 'Header text before the timer';
$_['entry_separator_timer']		= 'Text separators';
$_['entry_separator_timer_help']= 'XX : XX : XX : XX';
$_['entry_caption_timer']		= 'Captions of numeric cell';
$_['entry_caption_timer_help']	= 'XX day(s) XX hour(s) XX minute(s) XX second(s)';
$_['entry_countdays_timer']		= 'Count days quantity';
$_['entry_countdays_timer_help']= 'If days are hidden and this option "Disable" than hours would count days*24 + hours';
$_['entry_days_timer']			= 'Show days count';
$_['entry_sec_timer']			= 'Show seconds count';

// Customize
$_['entry_customize_title_size']		= 'Header font size';
$_['entry_customize_title_style']		= 'Header font style';
$_['entry_customize_title_color']		= 'Header font color';
$_['entry_customize_tablet_bgexists']	= 'Background of numeric cell exists';
$_['entry_customize_tablet_bgcolor']	= 'Background color of numeric cell';
$_['entry_customize_tablet_bggrad']		= 'Background gradient of numeric cell';
$_['entry_customize_tablet_brdexists']	= 'Border of numeric cell exists';
$_['entry_customize_tablet_brdcolor']	= 'Border color of numeric cell';
$_['entry_customize_tablet_brdradius']	= 'Border radius of numeric cell (pixels)';
$_['entry_customize_tablet_size']		= 'Font size in the numeric cell';
$_['entry_customize_tablet_style']		= 'Font style in the numeric cell';
$_['entry_customize_tablet_color']		= 'Font color in the numeric cell';
$_['entry_customize_separator_color']	= 'Separators font color';
$_['entry_tstyle_bold']					= 'Bold';
$_['entry_tstyle_italic']				= 'Italic';
$_['entry_tstyle_underline']			= 'Underline';

// Error
$_['error_permission']		= 'Warning: You do not have permission to modify module!';

// Help
$_['text_help']				= '<p><b>CountDown Timer v1.1 <a href="https://tramplin-studio.store/" target="blank">by Tramplin Studio</a></b> is a very flexible configuration module that displays a countdown timer for special promo goods on all possible pages and in all standard OpenCart modules. A wide range of design customization for the provides a beautiful display in any template.<br>
The countdown timer of the special promo goods will attract the attention of the visitors of the online store to the goods, thereby significantly increasing the conversion of the online store.</p>

<p>The module displays a numeric timer on the following pages and modules:<br>
<ul>
	<li>page "Product" (product/product);</li>
	<li>page "Category" (product/category);</li>
	<li>page "Manufacturer" (product/manufacturer);</li>
	<li>page "Special" (product/special);</li>
	<li>page "Search" (product/search);</li>
	<li>module "Latest" (module/latest);</li>
	<li>module "Bestsellers" (module/bestseller);</li>
	<li>module "Special" (module/special);</li>
	<li>module "Featured" (module/featured);</li>
	<li>module "Related" (product/product).</li>
</ul>
</p>

<p>For each page and module, you can singly configure a number of customize timer parameters:<br>
<ul>
	<li>display the timer on the page or in the module;</li>
	<li>the timer header: "Sale ends in";</li>
	<li>text separators (XX : XX : XX : XX);</li>
	<li>textual designations of quantities (XX days, XX hours, XX minutes, XX seconds);</li>
	<li>number of days (the textual designation of this value will also be hidden);</li>
	<li>number of days in hours (Example: 5 days * 24 hours = 120 hours + hours of the current day. Note: only works if the "number of days" is disabled);</li>
	<li>the number of seconds (the textual designation of this value will also be hidden).</li>
</ul>
</p>

<p>The timer for each singly promo goods is included when adding / editing the product in the "Special" tab.<br>
You must enter a promo price and the end date of the promotion. If you do not enter the end date, the countdown timer will not be displayed for this good.</p>

<p>If you want the timer of any goods to count the time before the end of the current day, then set the end date of the promotion "N days", turn off the option "Show days count", and enable "Count days quantity". For example, if the special promotion ends in 5 days, a countdown timer will be displayed every day for 5 days left time to until the end of the current day.<br>
Such a maneuver leads to an even greater increase in conversion for sale!</p>';

$_['text_author']			= '<p>This module is a completed product. Further updating of the module is unlikely.<br>See other modules of our development on our website:</p>
								<p>Site: <a href="https://tramplin-studio.store/" target="_blank">https://tramplin-studio.store/</a><br>
								E-mail: <a href="mailto:info@tramplin-studio.store">info@tramplin-studio.store</a></p>';
?>
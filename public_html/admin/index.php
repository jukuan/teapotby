<?php

// Version
use Sentry\SentrySdk;

define('VERSION', '3.0.3.8');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

if (!IS_PRODUCTION) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: ../install/index.php');
	exit;
}

require_once __DIR__ . '/../system/storage/vendor/autoload.php';

if (null === SentrySdk::getCurrentHub()->getClient()) {
    \Sentry\init([
        'dsn' => 'https://3cd52fdff1db467ea054cadf55702ebe@o294812.ingest.sentry.io/6049955'
    ]);
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('admin');
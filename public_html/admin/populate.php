<?php

// Show all errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Version
define('VERSION', '3.0.3.6');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');
require_once(DIR_SYSTEM . 'config/admin.php');
require_once(DIR_SYSTEM . 'config/catalog.php');
require_once(DIR_SYSTEM . 'engine/registry.php');

// Registry
$registry = new Registry();

// Config
$config = new Config();
$config->load('default');
$config->load('admin');

$config->get('db_name');

if (isset($application_config)) {
    $config->load($application_config);
}

$registry->set('db', new DB($config->get('db_engine'), $config->get('db_hostname'), $config->get('db_username'), $config->get('db_password'), $config->get('db_database'), $config->get('db_port')));
$registry->set('cache', new Cache($config->get('cache_engine'), $config->get('cache_expire')));


// Loader
$load = new Loader($registry);

$load->model('setting/store');
$load->model('catalog/product');
$load->model('catalog/category');
$load->model('catalog/information');

if (!class_exists('ModelCatalogProduct')) {
    die('Required class does not found');
}


$productsInfo = file_get_contents(__DIR__ . '/../product_fields.json');
$productsInfo = json_decode($productsInfo);

function truncateData(Registry $registry) {
    $db = $registry->get('db');
    $query = '
    TRUNCATE TABLE %oc%_category;
    TRUNCATE TABLE %oc%_category_description;
    TRUNCATE TABLE %oc%_category_filter;
    TRUNCATE TABLE %oc%_category_path;
    TRUNCATE TABLE %oc%_category_to_layout;
    TRUNCATE TABLE %oc%_category_to_store;
    
    TRUNCATE TABLE %oc%_product;
    TRUNCATE TABLE %oc%_product_attribute;
    TRUNCATE TABLE %oc%_product_description;
    TRUNCATE TABLE %oc%_product_discount;
    TRUNCATE TABLE %oc%_product_image;
    TRUNCATE TABLE %oc%_product_option;
    TRUNCATE TABLE %oc%_product_option_value;
    TRUNCATE TABLE %oc%_product_related;
    TRUNCATE TABLE %oc%_product_related;
    TRUNCATE TABLE %oc%_product_reward;
    TRUNCATE TABLE %oc%_product_special;
    TRUNCATE TABLE %oc%_product_tag;
    TRUNCATE TABLE %oc%_product_to_category;
    TRUNCATE TABLE %oc%_product_to_download;
    TRUNCATE TABLE %oc%_product_to_layout;
    TRUNCATE TABLE %oc%_product_to_store;
    TRUNCATE TABLE %oc%_review;
    DELETE FROM %oc%_url_alias WHERE query LIKE \'product_id=%\';
    ';
    $query = str_replace('%oc%_', DB_PREFIX, $query);
    $db->query($query);
}

function populateCategories(Registry $registry, $categories) {
//    $db = $registry->get('db');
//    $db->query();

    $categories = array_filter($categories, function ($catName) {
        $catName = trim($catName);

        return
            $catName !== 'Главная' ||
            mb_strtolower($catName) !== 'главная';
    });

    $ocCategoryHandler = new ModelCatalogCategory($registry);

    foreach ($categories as $title) {
        $ocCategoryHandler->addCategory([
            'top' => 0,
            'column' => 0,
            'parent_id' => 0,
            'sort_order' => 0,
            'status' => 1,
            'category_description' => [
                2 => [
                    'name' => $title,
                    'description' => '',
                    'meta_title' => $title,
                    'meta_description' => '',
                    'meta_keyword' => '',
                ],
            ]
        ]);
    }
}

$ocProductHandler = new ModelCatalogProduct($registry);


truncateData($registry);


foreach ($productsInfo as $productFields) {
    $title = $productFields->title;
    $text = $productFields->text;
    $props = $productFields->props;
    $categories = $productFields->categories;
    $image = $productFields->image;
    $imgPath = 'catalog/aq/' . basename($image);



    $model = $categories[1] ?? '';

    $ocProdProperties = [
        'model'           => $model,
        'image'           => $imgPath,
        'manufacturer_id' => 8,
        'language_id' => 8,

        'product_description' => [
            2 => [
                'language_id' => 8,
                'name' => $title,
                'description' => $text,
                'tag' => implode(',', $categories),
                'meta_title' => $title,
                'meta_description' => strip_tags($text),
                'meta_keyword' => '',
            ]
        ],

        'product_category' => [

        ],

        'price' => 50,
        'sku' => '',
        'upc' => '',
        'ean' => '',
        'jan' => '',
        'isbn' => '',
        'mpn' => '',
        'location' => '',
        'quantity' => 100,
        'minimum' => 1,
        'subtract' => 0,
        'stock_status_id' => 6, // 2-3 дня
        'date_available' => date('Y-m-d'),
        'shipping' => 1,
        'points' => 500,
        'weight_class_id' => 0,
        'weight' => 0.0,
        'width' => 0.0,
        'length' => 0.0,
        'height' => 0.0,
        'height_class' => '',
        'length_class_id' => 0,
        'status' => 1,
        'tax_class_id' => 0,
        'sort_order' => 0,
    ];

    $result = $ocProductHandler->addProduct($ocProdProperties);
}

die('-0-');

<?php
class ControllerExtensionModuleTSCountDownTimer extends Controller {
	private $error = array();

	public function index() {
		$extension_path = version_compare(VERSION, '3.0.0', '>=') ? "marketplace" : "extension";
		$token_var_name = version_compare(VERSION, '3.0.0', '>=') ? "user_token" : "token";
		
		$this->load->language('extension/module/ts_countdown_timer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('ts_countdown_timer', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link($extension_path . '/extension', $token_var_name . '=' . $this->session->data[$token_var_name] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
    	$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_settings_title_1'] = $this->language->get('text_settings_title_1');
		$data['text_settings_title_2'] = $this->language->get('text_settings_title_2');
		$data['text_customize_title_1'] = $this->language->get('text_customize_title_1');
		$data['text_customize_title_2'] = $this->language->get('text_customize_title_2');
		$data['text_timezone_default'] = $this->language->get('text_timezone_default');
		$data['text_timer_title'] = $this->language->get('text_timer_title');

		$data['tab_settings'] = $this->language->get('tab_settings');
		$data['tab_customize'] = $this->language->get('tab_customize');
		$data['tab_help'] = $this->language->get('tab_help');

		$data['entry_customize_title_size'] = $this->language->get('entry_customize_title_size');
		$data['entry_customize_title_style'] = $this->language->get('entry_customize_title_style');
		$data['entry_customize_title_color'] = $this->language->get('entry_customize_title_color');
		$data['entry_customize_tablet_bgexists'] = $this->language->get('entry_customize_tablet_bgexists');
		$data['entry_customize_tablet_bgcolor'] = $this->language->get('entry_customize_tablet_bgcolor');
		$data['entry_customize_tablet_bggrad'] = $this->language->get('entry_customize_tablet_bggrad');
		$data['entry_customize_tablet_brdexists'] = $this->language->get('entry_customize_tablet_brdexists');
		$data['entry_customize_tablet_brdcolor'] = $this->language->get('entry_customize_tablet_brdcolor');
		$data['entry_customize_tablet_brdradius'] = $this->language->get('entry_customize_tablet_brdradius');
		$data['entry_customize_tablet_size'] = $this->language->get('entry_customize_tablet_size');
		$data['entry_customize_tablet_style'] = $this->language->get('entry_customize_tablet_style');
		$data['entry_customize_tablet_color'] = $this->language->get('entry_customize_tablet_color');
		$data['entry_customize_separator_color'] = $this->language->get('entry_customize_separator_color');
		$data['entry_tstyle_bold'] = $this->language->get('entry_tstyle_bold');
		$data['entry_tstyle_italic'] = $this->language->get('entry_tstyle_italic');
		$data['entry_tstyle_underline'] = $this->language->get('entry_tstyle_underline');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_timezone'] = $this->language->get('entry_timezone');
		$data['entry_title_timer'] = $this->language->get('entry_title_timer');
		$data['entry_title_text_timer'] = $this->language->get('entry_title_text_timer');
		$data['entry_separator_timer'] = $this->language->get('entry_separator_timer');
		$data['entry_separator_timer_help'] = $this->language->get('entry_separator_timer_help');
		$data['entry_caption_timer'] = $this->language->get('entry_caption_timer');
		$data['entry_caption_timer_help'] = $this->language->get('entry_caption_timer_help');
		$data['entry_countdays_timer'] = $this->language->get('entry_countdays_timer');
		$data['entry_countdays_timer_help'] = $this->language->get('entry_countdays_timer_help');
		$data['entry_days_timer'] = $this->language->get('entry_days_timer');
		$data['entry_sec_timer'] = $this->language->get('entry_sec_timer');

		$data['text_help'] = $this->language->get('text_help');
		$data['text_author'] = $this->language->get('text_author');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_enable_all'] = $this->language->get('button_enable_all');
		$data['button_disable_all'] = $this->language->get('button_disable_all');


 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_var_name . '=' . $this->session->data[$token_var_name], true),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_extension'),
			'href'      => $this->url->link($extension_path . '/extension', $token_var_name . '=' . $this->session->data[$token_var_name] . '&type=module', true),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/ts_countdown_timer', $token_var_name . '=' . $this->session->data[$token_var_name], true),
      		'separator' => ' :: '
   		);

		$data['action'] = $this->url->link('extension/module/ts_countdown_timer', $token_var_name . '=' . $this->session->data[$token_var_name], true);

		$data['cancel'] = $this->url->link($extension_path . '/extension', $token_var_name . '=' . $this->session->data[$token_var_name] . '&type=module', true);


		$timer_positions = array(
			'product', 
			'category', 
			'manufacturer', 
			'special', 
			'search', 
			'latest', 
			'bestseller', 
			'specialmod',
			'featured', 
			'related', 
		);
		
		$server_timezone = (int)date('O') / 100;
		
		$data['timer_positions'] = $timer_positions;
		$data['server_timezone'] = $server_timezone;
		
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		
		if (isset($this->request->post['ts_countdown_timer_status'])) {
			$data['ts_countdown_timer_status'] = $this->request->post['ts_countdown_timer_status'];
		} elseif ($this->config->get('ts_countdown_timer_status')) {
			$data['ts_countdown_timer_status'] = $this->config->get('ts_countdown_timer_status');
		} else {
			$data['ts_countdown_timer_status'] = '';
		}
		
		if (isset($this->request->post['ts_countdown_timer_settings'])) {
			$data['ts_countdown_timer_settings'] = $this->request->post['ts_countdown_timer_settings'];
		} elseif ($this->config->get('ts_countdown_timer_settings')) {
			$data['ts_countdown_timer_settings'] = $this->config->get('ts_countdown_timer_settings');
		} else {
			$data['ts_countdown_timer_settings'] = array(
				'timezone'		=> $server_timezone
			);
			foreach ($timer_positions as $timer_position) {
				$data['ts_countdown_timer_settings'][$timer_position] = array(
					'status'		=> 1,
					'title'			=> 1,
					'title_text'	=> array(),
					'separator'		=> 1,
					'caption'		=> 1,
					'days'			=> 1,
					'countdays'		=> 0,
					'seconds'		=> 1,
				);
			}
		}
		
		if (isset($this->request->post['ts_countdown_timer_customize'])) {
			$data['ts_countdown_timer_customize'] = $this->request->post['ts_countdown_timer_customize'];
		} elseif ($this->config->get('ts_countdown_timer_customize')) {
			$data['ts_countdown_timer_customize'] = $this->config->get('ts_countdown_timer_customize');
		} else {
			$data['ts_countdown_timer_customize'] = array(
				'title_size'		=> 12,
				'title_style'		=> array(
					'b' => 0,
					'i' => 0,
					'u' => 0
				),
				'title_color'		=> '#616161',
				'tablet_bgexists'	=> 1,
				'tablet_bgcolor'	=> '#cccccc',
				'tablet_bggrad'		=> 1,
				'tablet_brdexists'	=> 1,
				'tablet_brdcolor'	=> '#919191',
				'tablet_brdradius'	=> 3,
				'tablet_size'		=> 28,
				'tablet_style'		=> array(
					'b' => 1,
					'i' => 0,
					'u' => 0
				),
				'tablet_color'		=> '#616161',
				'separator_color'	=> '#616161'
			);
		}



		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/ts_countdown_timer', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/ts_countdown_timer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
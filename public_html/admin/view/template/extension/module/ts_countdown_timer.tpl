<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
		
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_status"><?php echo $entry_status; ?></label>
			<div class="col-sm-10">
				<select name="ts_countdown_timer_status" id="ts_countdown_timer_status" class="form-control">
					<?php if ($ts_countdown_timer_status) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				</select>
			</div>
		  </div>
		  <br>
		  
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-settings" data-toggle="tab"><?php echo $tab_settings[0]; ?></a></li>
            <li><a href="#tab-customize" data-toggle="tab"><?php echo $tab_customize; ?></a></li>
            <li><a href="#tab-help" data-toggle="tab"><?php echo $tab_help; ?></a></li>
          </ul>
	      <div class="tab-content">

<div class="tab-pane active" id="tab-settings">
<fieldset>
	<legend><?php echo $text_settings_title_1; ?></legend>
	<ul class="col-md-3 nav nav-pills nav-stacked">
		<?php foreach($timer_positions as $pid => $position) { ?>
		<li <?php if($pid==0) { ?>class="active"<?php } ?>><a href="#tab-settings-<?php echo $position; ?>" data-toggle="tab"><?php echo $tab_settings[$position]; ?></a></li>
		<?php } ?>
	</ul>
	<div class="col-md-9 tab-content" style="border-left:1px solid #ededed;">
		<?php foreach($timer_positions as $pid => $position) { ?>
		<div class="tab-pane <?php if($pid==0) { ?>active<?php } ?>" id="tab-settings-<?php echo $position; ?>">
			<div class="form-group" style="padding-top:0;">
				<div class="pull-right container-fluid">
					<button type="button" class="btn btn-success" onclick="enableAllSelects('<?php echo $position; ?>');"><?php echo $button_enable_all; ?></button>
					<button type="button" class="btn btn-danger" onclick="disableAllSelects('<?php echo $position; ?>');"><?php echo $button_disable_all; ?></button>
				</div>
				<legend style="border:0; margin:5px 0 0 25px;"><?php echo $tab_settings[$position]; ?></legend>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>"><?php echo $entry_status; ?></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][status]" id="ts_countdown_timer_<?php echo $position; ?>" class="form-control">
					<?php if (isset($ts_countdown_timer_settings[$position]['status']) && $ts_countdown_timer_settings[$position]['status']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_title"><?php echo $entry_title_timer; ?></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][title]" id="ts_countdown_timer_<?php echo $position; ?>_title" class="form-control" onchange="titleTextEnable('<?php echo $position; ?>');">
					<?php if (isset($ts_countdown_timer_settings[$position]['title']) && $ts_countdown_timer_settings[$position]['title']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_title_text"><?php echo $entry_title_text_timer; ?></label>
				<div class="col-sm-9">
					<?php foreach ($languages as $language) { ?>
					<div class="input-group" style="margin-bottom: 10px;">
						<span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
						<input type="text" name="ts_countdown_timer_settings[<?php echo $position; ?>][title_text][<?php echo $language['language_id']; ?>]" id="ts_countdown_timer_<?php echo $position; ?>_title_text_<?php echo $language['language_id']; ?>" value="<?php echo isset($ts_countdown_timer_settings[$position]['title_text'][$language['language_id']]) ? $ts_countdown_timer_settings[$position]['title_text'][$language['language_id']] : $text_timer_title; ?>" class="form-control" />
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_separator"><span data-toggle="tooltip" title="<?php echo $entry_separator_timer_help; ?>"><?php echo $entry_separator_timer; ?></span></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][separator]" id="ts_countdown_timer_<?php echo $position; ?>_separator" class="form-control">
					<?php if (isset($ts_countdown_timer_settings[$position]['separator']) && $ts_countdown_timer_settings[$position]['separator']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_caption"><span data-toggle="tooltip" title="<?php echo $entry_caption_timer_help; ?>"><?php echo $entry_caption_timer; ?></span></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][caption]" id="ts_countdown_timer_<?php echo $position; ?>_caption" class="form-control">
					<?php if (isset($ts_countdown_timer_settings[$position]['caption']) && $ts_countdown_timer_settings[$position]['caption']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_days"><?php echo $entry_days_timer; ?></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][days]" id="ts_countdown_timer_<?php echo $position; ?>_days" class="form-control" onchange="countdaysEnable('<?php echo $position; ?>');">
					<?php if (isset($ts_countdown_timer_settings[$position]['days']) && $ts_countdown_timer_settings[$position]['days']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_countdays"><span data-toggle="tooltip" title="<?php echo $entry_countdays_timer_help; ?>"><?php echo $entry_countdays_timer; ?></span></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][countdays]" id="ts_countdown_timer_<?php echo $position; ?>_countdays" class="form-control">
					<?php if (isset($ts_countdown_timer_settings[$position]['countdays']) && $ts_countdown_timer_settings[$position]['countdays']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="ts_countdown_timer_<?php echo $position; ?>_seconds"><?php echo $entry_sec_timer; ?></label>
				<div class="col-sm-9">
					<select name="ts_countdown_timer_settings[<?php echo $position; ?>][seconds]" id="ts_countdown_timer_<?php echo $position; ?>_seconds" class="form-control">
					<?php if (isset($ts_countdown_timer_settings[$position]['seconds']) && $ts_countdown_timer_settings[$position]['seconds']) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</fieldset>
<br>
<fieldset>
	<legend><?php echo $text_settings_title_2; ?></legend>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="ts_countdown_timer_timezone"><?php echo $entry_timezone; ?></label>
		<div class="col-sm-10">
			<select name="ts_countdown_timer_settings[timezone]" id="ts_countdown_timer_timezone" class="form-control">
				<?php for($i = 12; $i >= 0; $i--) { ?>
				<option value="+<?php echo $i; ?>" <?php if (isset($ts_countdown_timer_settings['timezone']) && $ts_countdown_timer_settings['timezone'] == $i) { ?>selected="selected"<?php } ?>>GMT+<?php echo ($i <= 9) ? '0'.$i : $i; ?>:00 <?php if ($i == $server_timezone) { echo $text_timezone_default; } ?></option>
				<?php } ?>
				<?php for($i = 1; $i <= 12; $i++) { ?>
				<option value="-<?php echo $i; ?>" <?php if (isset($ts_countdown_timer_settings['timezone']) && $ts_countdown_timer_settings['timezone'] == intval('-'.$i)) { ?>selected="selected"<?php } ?>>GMT-<?php echo ($i <= 9) ? '0'.$i : $i; ?>:00 <?php if (intval('-'.$i) == $server_timezone) { echo $text_timezone_default; } ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
</fieldset>
<br>
</div>

<div class="tab-pane" id="tab-customize">
	<fieldset>
		<legend><?php echo $text_customize_title_1; ?></legend>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_title_size"><?php echo $entry_customize_title_size; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[title_size]" id="ts_countdown_timer_customize_title_size" value="<?php echo isset($ts_countdown_timer_customize['title_size']) ? $ts_countdown_timer_customize['title_size'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_title_style"><?php echo $entry_customize_title_style; ?></label>
			<div class="col-sm-10">
				<input type="checkbox" name="ts_countdown_timer_customize[title_style][b]" id="ts_countdown_timer_customize_title_style_b" value="1" <?php echo (isset($ts_countdown_timer_customize['title_style']['b']) && $ts_countdown_timer_customize['title_style']['b']!=0) ? 'checked' : ''; ?>>&nbsp;<label for="ts_countdown_timer_customize_title_style_b" style="font-weight:bold;"><?php echo $entry_tstyle_bold; ?></label><br>
				<input type="checkbox" name="ts_countdown_timer_customize[title_style][i]" id="ts_countdown_timer_customize_title_style_i" value="1" <?php echo (isset($ts_countdown_timer_customize['title_style']['i']) && $ts_countdown_timer_customize['title_style']['i']!=0) ? 'checked' : ''; ?>>&nbsp;<label for="ts_countdown_timer_customize_title_style_i" style="font-weight:normal;font-style:italic;"><?php echo $entry_tstyle_italic; ?></label><br>
				<input type="checkbox" name="ts_countdown_timer_customize[title_style][u]" id="ts_countdown_timer_customize_title_style_u" value="1" <?php echo (isset($ts_countdown_timer_customize['title_style']['u']) && $ts_countdown_timer_customize['title_style']['u']!=0) ? 'checked' : ''; ?>>&nbsp;<label for="ts_countdown_timer_customize_title_style_u" style="font-weight:normal;text-decoration:underline;"><?php echo $entry_tstyle_underline; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_title_color"><?php echo $entry_customize_title_color; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[title_color]" id="ts_countdown_timer_customize_title_color" value="<?php echo isset($ts_countdown_timer_customize['title_color']) ? $ts_countdown_timer_customize['title_color'] : ''; ?>" class="form-control" />
			</div>
		</div>
	</fieldset>
	<br>
	<fieldset>
		<legend><?php echo $text_customize_title_2; ?></legend>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_size"><?php echo $entry_customize_tablet_size; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[tablet_size]" id="ts_countdown_timer_customize_tablet_size" value="<?php echo isset($ts_countdown_timer_customize['tablet_size']) ? $ts_countdown_timer_customize['tablet_size'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_style"><?php echo $entry_customize_tablet_style; ?></label>
			<div class="col-sm-10">
				<input type="checkbox" name="ts_countdown_timer_customize[tablet_style][b]" id="ts_countdown_timer_customize_tablet_style_b" value="1" <?php echo (isset($ts_countdown_timer_customize['tablet_style']['b']) && $ts_countdown_timer_customize['tablet_style']['b']!=0) ? 'checked' : ''; ?>>&nbsp;<label for="ts_countdown_timer_customize_tablet_style_b" style="font-weight:bold;"><?php echo $entry_tstyle_bold; ?></label><br>
				<input type="checkbox" name="ts_countdown_timer_customize[tablet_style][i]" id="ts_countdown_timer_customize_tablet_style_i" value="1" <?php echo (isset($ts_countdown_timer_customize['tablet_style']['i']) && $ts_countdown_timer_customize['tablet_style']['i']!=0) ? 'checked' : ''; ?>>&nbsp;<label for="ts_countdown_timer_customize_tablet_style_i" style="font-weight:normal;font-style:italic;"><?php echo $entry_tstyle_italic; ?></label><br>
				<input type="checkbox" name="ts_countdown_timer_customize[tablet_style][u]" id="ts_countdown_timer_customize_tablet_style_u" value="1" <?php echo (isset($ts_countdown_timer_customize['tablet_style']['u']) && $ts_countdown_timer_customize['tablet_style']['u']!=0) ? 'checked' : ''; ?>>&nbsp;<label for="ts_countdown_timer_customize_tablet_style_u" style="font-weight:normal;text-decoration:underline;"><?php echo $entry_tstyle_underline; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_color"><?php echo $entry_customize_tablet_color; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[tablet_color]" id="ts_countdown_timer_customize_tablet_color" value="<?php echo isset($ts_countdown_timer_customize['tablet_color']) ? $ts_countdown_timer_customize['tablet_color'] : ''; ?>" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_separator_color"><?php echo $entry_customize_separator_color; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[separator_color]" id="ts_countdown_timer_customize_separator_color" value="<?php echo isset($ts_countdown_timer_customize['separator_color']) ? $ts_countdown_timer_customize['separator_color'] : ''; ?>" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_bgexists"><?php echo $entry_customize_tablet_bgexists; ?></label>
			<div class="col-sm-10">
				<select name="ts_countdown_timer_customize[tablet_bgexists]" id="ts_countdown_timer_customize_tablet_bgexists" class="form-control">
				<?php if (isset($ts_countdown_timer_customize['tablet_bgexists']) && $ts_countdown_timer_customize['tablet_bgexists']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_bgcolor"><?php echo $entry_customize_tablet_bgcolor; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[tablet_bgcolor]" id="ts_countdown_timer_customize_tablet_bgcolor" value="<?php echo isset($ts_countdown_timer_customize['tablet_bgcolor']) ? $ts_countdown_timer_customize['tablet_bgcolor'] : ''; ?>" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_bggrad"><?php echo $entry_customize_tablet_bggrad; ?></label>
			<div class="col-sm-10">
				<select name="ts_countdown_timer_customize[tablet_bggrad]" id="ts_countdown_timer_customize_tablet_bggrad" class="form-control">
				<?php if (isset($ts_countdown_timer_customize['tablet_bggrad']) && $ts_countdown_timer_customize['tablet_bggrad']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_brdexists"><?php echo $entry_customize_tablet_brdexists; ?></label>
			<div class="col-sm-10">
				<select name="ts_countdown_timer_customize[tablet_brdexists]" id="ts_countdown_timer_customize_tablet_brdexists" class="form-control">
				<?php if (isset($ts_countdown_timer_customize['tablet_brdexists']) && $ts_countdown_timer_customize['tablet_brdexists']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_brdcolor"><?php echo $entry_customize_tablet_brdcolor; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[tablet_brdcolor]" id="ts_countdown_timer_customize_tablet_brdcolor" value="<?php echo isset($ts_countdown_timer_customize['tablet_brdcolor']) ? $ts_countdown_timer_customize['tablet_brdcolor'] : ''; ?>" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ts_countdown_timer_customize_tablet_brdradius"><?php echo $entry_customize_tablet_brdradius; ?></label>
			<div class="col-sm-10">
				<input type="text" name="ts_countdown_timer_customize[tablet_brdradius]" id="ts_countdown_timer_customize_tablet_brdradius" value="<?php echo isset($ts_countdown_timer_customize['tablet_brdradius']) ? $ts_countdown_timer_customize['tablet_brdradius'] : ''; ?>" />
			</div>
		</div>
	<fieldset>
</div>

<div class="tab-pane" id="tab-help">
	<div class="form-group">
		<div class="col-sm-12">
			<? echo $text_help; ?>
		</div>
	</div>
	<div class="form-group text-left">
		<div class="col-sm-12">
			<? echo $text_author; ?>
		</div>
	</div>
</div>

		</div>
		</form>
	  </div>
	</div>
  </div>
</div>
<link type="text/css" href="view/javascript/tramplin-studio/CountdownTimer/ion-rangeslider/ion.rangeSlider.css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/javascript/tramplin-studio/CountdownTimer/ion-rangeslider/ion.rangeSlider.skinModern.css" rel="stylesheet" media="screen" />
<script type="text/javascript" src="view/javascript/tramplin-studio/CountdownTimer/ion-rangeslider/ion.rangeSlider.min.js"></script>
<link type="text/css" href="view/javascript/tramplin-studio/CountdownTimer/colorpicker/colorpicker.css" rel="stylesheet" media="screen" />
<script type="text/javascript" src="view/javascript/tramplin-studio/CountdownTimer/colorpicker/colorpicker.js"></script>
<script type="text/javascript">
function enableAllSelects(tabId) {
	$("#tab-settings-"+tabId+" select").each(function(){
		$(this).find("option").each(function(){
			if($(this).val()==1) {
				$(this).prop('selected', true);
			}
		});
	});
	titleTextEnable(tabId);
	countdaysEnable(tabId);
}
function disableAllSelects(tabId) {
	$("#tab-settings-"+tabId+" select").each(function(){
		$(this).find("option").each(function(){
			if($(this).val()==0) {
				$(this).prop('selected', true);
			}
		});
	});
	titleTextEnable(tabId);
	countdaysEnable(tabId);
}
function titleTextEnable(tabId) {
	<?php foreach ($languages as $language) { ?>
	if($("#ts_countdown_timer_"+tabId+"_title").val()!=0) {
		$("#ts_countdown_timer_"+tabId+"_title_text_<?php echo $language['language_id']; ?>").prop('disabled', false);
	} else {
		$("#ts_countdown_timer_"+tabId+"_title_text_<?php echo $language['language_id']; ?>").prop('disabled', true);
	}
	<?php } ?>
}
function countdaysEnable(tabId) {
	if($("#ts_countdown_timer_"+tabId+"_days").val()==0) {
		$("#ts_countdown_timer_"+tabId+"_countdays").prop('disabled', false);
	} else {
		$("#ts_countdown_timer_"+tabId+"_countdays").prop('disabled', true);
		$("#ts_countdown_timer_"+tabId+"_countdays option").each(function(){
			if($(this).val()==0) {
				$(this).prop('selected', true);
			}
		});
	}
}
$(function() {
	<?php foreach($timer_positions as $pid => $position) { ?>
	titleTextEnable('<?php echo $position; ?>');
	countdaysEnable('<?php echo $position; ?>');
	<?php } ?>
});
$(function() {
	$("#ts_countdown_timer_customize_title_size").ionRangeSlider({
		min: 10,
		max: 20,
		step: 1,
		grid: true,
		grid_num: 10
	});
	$("#ts_countdown_timer_customize_tablet_brdradius").ionRangeSlider({
		min: 0,
		max: 20,
		step: 1,
		grid: true,
		grid_num: 20
	});
	$("#ts_countdown_timer_customize_tablet_size").ionRangeSlider({
		min: 20,
		max: 40,
		step: 2,
		grid: true,
		grid_num: 20
	});
	$('#ts_countdown_timer_customize_title_color').css('background-color', '<?php echo $ts_countdown_timer_customize['title_color']; ?>');
	$("#ts_countdown_timer_customize_title_color").ColorPicker({
		color: '<?php echo $ts_countdown_timer_customize['title_color']; ?>',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#ts_countdown_timer_customize_title_color').css('background-color', '#' + hex);
			$("#ts_countdown_timer_customize_title_color").val('#' + hex);
		}
	});
	$('#ts_countdown_timer_customize_tablet_bgcolor').css('background-color', '<?php echo $ts_countdown_timer_customize['tablet_bgcolor']; ?>');
	$("#ts_countdown_timer_customize_tablet_bgcolor").ColorPicker({
		color: '<?php echo $ts_countdown_timer_customize['tablet_bgcolor']; ?>',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#ts_countdown_timer_customize_tablet_bgcolor').css('background-color', '#' + hex);
			$("#ts_countdown_timer_customize_tablet_bgcolor").val('#' + hex);
		}
	});
	$('#ts_countdown_timer_customize_tablet_brdcolor').css('background-color', '<?php echo $ts_countdown_timer_customize['tablet_brdcolor']; ?>');
	$("#ts_countdown_timer_customize_tablet_brdcolor").ColorPicker({
		color: '<?php echo $ts_countdown_timer_customize['tablet_brdcolor']; ?>',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#ts_countdown_timer_customize_tablet_brdcolor').css('background-color', '#' + hex);
			$("#ts_countdown_timer_customize_tablet_brdcolor").val('#' + hex);
		}
	});
	$('#ts_countdown_timer_customize_tablet_color').css('background-color', '<?php echo $ts_countdown_timer_customize['tablet_color']; ?>');
	$("#ts_countdown_timer_customize_tablet_color").ColorPicker({
		color: '<?php echo $ts_countdown_timer_customize['tablet_color']; ?>',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#ts_countdown_timer_customize_tablet_color').css('background-color', '#' + hex);
			$("#ts_countdown_timer_customize_tablet_color").val('#' + hex);
		}
	});
	$('#ts_countdown_timer_customize_separator_color').css('background-color', '<?php echo $ts_countdown_timer_customize['separator_color']; ?>');
	$("#ts_countdown_timer_customize_separator_color").ColorPicker({
		color: '<?php echo $ts_countdown_timer_customize['separator_color']; ?>',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#ts_countdown_timer_customize_separator_color').css('background-color', '#' + hex);
			$("#ts_countdown_timer_customize_separator_color").val('#' + hex);
		}
	});
});
</script>

<?php echo $footer; ?>
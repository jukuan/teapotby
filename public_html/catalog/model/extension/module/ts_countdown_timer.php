<?php
class ModelExtensionModuleTSCountDownTimer extends Model {
	public function getTimer($product_id, $parent) {
		if ($this->config->get('ts_countdown_timer_status')) {
			$this->load->language('extension/module/ts_countdown_timer');
			
			$special = $this->getSpecialData($product_id);
			if($special['date_end'] && time() < strtotime($special['date_end']) && isset($this->config->get('ts_countdown_timer_settings')[$parent]['status']) && $this->config->get('ts_countdown_timer_settings')[$parent]['status']){
				
				$timezone = (isset($this->config->get('ts_countdown_timer_settings')['timezone']) && $this->config->get('ts_countdown_timer_settings')['timezone']) ? (int)$this->config->get('ts_countdown_timer_settings')['timezone'] : 0;
				
				$data['product_id'] = (int)$product_id;
				$data['parent'] = $parent;
				$data['date'] = date('Y-m-d\TH:i:s', strtotime($special['date_end']) - (((int)date('O') / 100) * 60 * 60) + ($timezone * 60 * 60));
				$data['language'] = $this->config->get('config_language');
				$data['days'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['days']) && $this->config->get('ts_countdown_timer_settings')[$parent]['days']) ? 1 : 0;
				$data['countdays'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['countdays']) && $this->config->get('ts_countdown_timer_settings')[$parent]['countdays']) ? 1 : 0;
				$data['seconds'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['seconds']) && $this->config->get('ts_countdown_timer_settings')[$parent]['seconds']) ? 1 : 0;
				$data['separator'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['separator']) && $this->config->get('ts_countdown_timer_settings')[$parent]['separator']) ? 1 : 0;
				$data['title']['status'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['title']) && $this->config->get('ts_countdown_timer_settings')[$parent]['title']) ? 1 : 0;
				$data['title']['text'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['title_text'][$this->config->get('config_language_id')]) && $this->config->get('ts_countdown_timer_settings')[$parent]['title_text'][$this->config->get('config_language_id')]) ? $this->config->get('ts_countdown_timer_settings')[$parent]['title_text'][$this->config->get('config_language_id')] : '';
				$data['caption']['status'] = (isset($this->config->get('ts_countdown_timer_settings')[$parent]['caption']) && $this->config->get('ts_countdown_timer_settings')[$parent]['caption']) ? 1 : 0;
				$data['caption']['text']['days'] = $this->language->get('text_countdown_days');
				$data['caption']['text']['hours'] = $this->language->get('text_countdown_hours');
				$data['caption']['text']['minutes'] = $this->language->get('text_countdown_minutes');
				$data['caption']['text']['seconds'] = $this->language->get('text_countdown_seconds');
				
				return json_encode($data, JSON_UNESCAPED_UNICODE);
			}
		}
		
		return false;
	}
	
	public function getCustomize() {
		if ($this->config->get('ts_countdown_timer_status')) {
			$customize = array(
				'title_size' => (isset($this->config->get('ts_countdown_timer_customize')['title_size']) && $this->config->get('ts_countdown_timer_customize')['title_size']) ? $this->config->get('ts_countdown_timer_customize')['title_size'] : 0,
				'title_style' => (isset($this->config->get('ts_countdown_timer_customize')['title_style']) && $this->config->get('ts_countdown_timer_customize')['title_style']) ? $this->config->get('ts_countdown_timer_customize')['title_style'] : 0,
				'title_color' => (isset($this->config->get('ts_countdown_timer_customize')['title_color']) && $this->config->get('ts_countdown_timer_customize')['title_color']) ? $this->config->get('ts_countdown_timer_customize')['title_color'] : 0,
				'tablet_bgexists' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_bgexists']) && $this->config->get('ts_countdown_timer_customize')['tablet_bgexists']) ? $this->config->get('ts_countdown_timer_customize')['tablet_bgexists'] : 0,
				'tablet_bgcolor' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_bgcolor']) && $this->config->get('ts_countdown_timer_customize')['tablet_bgcolor']) ? $this->config->get('ts_countdown_timer_customize')['tablet_bgcolor'] : 0,
				'tablet_bggrad' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_bggrad']) && $this->config->get('ts_countdown_timer_customize')['tablet_bggrad']) ? $this->config->get('ts_countdown_timer_customize')['tablet_bggrad'] : 0,
				'tablet_brdexists' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_brdexists']) && $this->config->get('ts_countdown_timer_customize')['tablet_brdexists']) ? $this->config->get('ts_countdown_timer_customize')['tablet_brdexists'] : 0,
				'tablet_brdcolor' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_brdcolor']) && $this->config->get('ts_countdown_timer_customize')['tablet_brdcolor']) ? $this->config->get('ts_countdown_timer_customize')['tablet_brdcolor'] : 0,
				'tablet_brdradius' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_brdradius']) && $this->config->get('ts_countdown_timer_customize')['tablet_brdradius']) ? $this->config->get('ts_countdown_timer_customize')['tablet_brdradius'] : 0,
				'tablet_size' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_size']) && $this->config->get('ts_countdown_timer_customize')['tablet_size']) ? $this->config->get('ts_countdown_timer_customize')['tablet_size'] : 0,
				'tablet_style' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_style']) && $this->config->get('ts_countdown_timer_customize')['tablet_style']) ? $this->config->get('ts_countdown_timer_customize')['tablet_style'] : 0,
				'tablet_color' => (isset($this->config->get('ts_countdown_timer_customize')['tablet_color']) && $this->config->get('ts_countdown_timer_customize')['tablet_color']) ? $this->config->get('ts_countdown_timer_customize')['tablet_color'] : 0,
				'separator_color' => (isset($this->config->get('ts_countdown_timer_customize')['separator_color']) && $this->config->get('ts_countdown_timer_customize')['separator_color']) ? $this->config->get('ts_countdown_timer_customize')['separator_color'] : 0,
			);
			return json_encode($customize);
		}
		
		return false;
	}
	
	private function getSpecialData($product_id) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$query = $this->db->query("SELECT `price`, `date_end` FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

		if ($query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
	}
}
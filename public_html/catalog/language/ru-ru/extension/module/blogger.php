<?php
// Heading 
$_['heading_title']    = 'Последние статьи';
$_['heading_description'] = 'Следите за нашими последними обновлениями, акциями, скидками и новостями компании!';

// Text
$_['text_read_more']   = 'Читать';
$_['text_date_added']  = 'Дата:';
$_['entry_comment']       = 'Комментарии';

// Button
$_['button_all_blogs'] = 'Открыть все статьи';

// Heading_text
$_['heading_text']      = 'Mirum est notare quam littera gothica quam nunc putamus parum claram!';
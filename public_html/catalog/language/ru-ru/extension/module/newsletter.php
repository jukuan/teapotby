<?php
// Heading
$_['heading_title']     = 'Рассылка';

// Entry
$_['entry_email']          = 'Укажите Ваш e-mail';

// Email
$_['email_subject']       = 'Добро пожаловать!';
$_['email_content']       = 'Спасибо за подписку';

// Button
$_['email_button']       = 'Подписаться';


//Description
$_['newsletter_info']     = 'Присоединяйтесь! Все новинки';
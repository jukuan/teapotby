<?php
// Heading
$_['heading_title']       = 'Статьи';

// Text
$_['text_blogs']          = 'Все статьи';
$_['text_success']        = 'You have successfully added your comment!';
$_['text_approval']       = 'You have successfully added your comment and it is awaiting approval!';
$_['text_no_blogs']       = 'Пока нет статей, но не переключайтесь!';
$_['text_read_more']      = 'Далее';
$_['text_date_added']     = 'Дата добавления';
$_['text_login_required'] = 'You must <a href="%s">login</a> to add a comment. If you do not have an account, you may <a href="%s">register</a> for one. Registration is free!';
$_['text_your_comments']  = 'Your Comments';
$_['text_error']       	  = 'Blog not found!';

// Entry
$_['entry_author']        = 'Автор';
$_['entry_email']         = 'E-Mail адрес';
$_['entry_comment']       = '';

// Column
$_['column_title']        = 'Заголовок';

// Button
$_['button_comment_add']  = 'Ваш комментарий';
$_['button_list_blogs']   = 'Спісок всех новостей';
$_['button_view']         = 'View Blog';

// Email
$_['email_subject']       = 'Blog Comment: %s';
$_['email_content']       = 'A comment has been made regarding the blog titled "%s".';

// Error
$_['error_author']        = 'Author name must be between 1 and 32 characters!';
$_['error_email']         = 'E-Mail Address does not appear to be valid!';
$_['error_comment']       = 'Comment must be between 10 and 3000 characters!';


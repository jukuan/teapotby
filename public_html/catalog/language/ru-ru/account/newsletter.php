<?php
// Heading
$_['heading_title']    = 'Подписка на статьи';

// Text
$_['text_account']     = 'Личный Кабинет';
$_['text_newsletter']  = 'Рассылка';
$_['text_success']     = 'Ваша подписка успешно обновлена!';

// Entry
$_['entry_newsletter'] = 'Подписаться';


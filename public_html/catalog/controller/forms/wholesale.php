<?php

declare(strict_types=1);

class ControllerFormsWholesale extends Controller
{
    public function index(): void
    {
        $this->document->setTitle($this->language->get('Чай, Кофе &ndash; Оптом'));

        $data = [];
        $data['header_custom'] = $this->load->controller('common/header');
        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('forms/wholesale', $data));
    }
}

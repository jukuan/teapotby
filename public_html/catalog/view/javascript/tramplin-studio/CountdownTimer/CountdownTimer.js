$.fn.extend({
	TSCountDownTimer: function(options) {
		var started = false;
		var options = $.extend({
			parent:		'product',
			product_id:	0,
			date:		'2022-01-01T00:00:00',
			language:	'en',
			days:		true,
			countdays:	true,
			seconds:	true,
			separator:	true,
			title:		{
				status:	true,
				text:	'',
			},
			caption:	{
				status:	true,
				text:	{
					days:      { 1:" day ", 2:" days ", 3: " days " },
					hours:     { 1:" hour ", 2: " hours ", 3: " hours " },
					minutes:   { 1:" minute ", 2: " minutes ", 3: " minutes " },
					seconds:   { 1:" second ", 2: " seconds ", 3: " seconds " },
				}
			},
		}, options);

		function getCaption(time, caption) {
			time = parseInt(time);
			if(options.language == "en") {
				if(time == 1) {
					return caption[0];
				} else {
					return caption[1];
				}
			} else {
				var slice = parseInt(String(time).slice(-1));
				if(slice == 1 && (time < 10 || time > 20)) {
					return caption[0];
				} else if((slice >= 2 && slice <= 4) && (time < 10 || time > 20)) {
					return caption[1];
				} else {
					return caption[2];
				}
			}
		}

		function getTimerValues() {
			var today = new Date();
			var endDay = new Date(options.date);
			var tLeft = (endDay.getTime() - today.getTime());
			
			if (tLeft > 0) {
				var dLeft = 0;
				var dLeftS = tLeft / 86400000;
				if(options.days || options.countdays) {
					dLeft = Math.floor(dLeftS);
					var dCaption = getCaption(dLeft, options.caption.text.days);
				}
				
				var hLeftS = (dLeftS - dLeft) * 24;
				var hLeft = Math.floor(hLeftS);
				if(hLeft < 10) {
					hLeft = '0' + hLeft;
				}
				var hCaption = getCaption(hLeft, options.caption.text.hours);
				
				var mLeftS = (hLeftS - hLeft) * 60;
				var mLeft = Math.floor(mLeftS);
				if(mLeft < 10) {
					mLeft = '0' + mLeft;
				}
				var mCaption = getCaption(mLeft, options.caption.text.minutes);
				
				var sLeft = Math.floor((mLeftS - mLeft) * 60);
				if(sLeft < 10) {
					sLeft = '0' + sLeft;
				}
				var sCaption = getCaption(sLeft, options.caption.text.seconds);
				
				return {
					days:		{ value: dLeft, caption: dCaption },
					hours:		{ value: hLeft, caption: hCaption },
					minutes:	{ value: mLeft, caption: mCaption },
					seconds:	{ value: sLeft, caption: sCaption }
				};
			} else {
				return false;
			}
		}
		
		function setTimerValues() {
			var timerValues = getTimerValues();
			if(timerValues) {
				if(options.days) {
					$("#cdt-" + options.parent + '-' + options.product_id + " .cdtDays div:first-child span").html(timerValues.days.value);
					if(options.caption.status) { 
						$("#cdt-" + options.parent + '-' + options.product_id + " .cdtDays div:last-child").html(timerValues.days.caption);
					}
				}
				$("#cdt-" + options.parent + '-' + options.product_id + " .cdtHrs div:first-child span").html(timerValues.hours.value);
				if(options.caption.status) {
					$("#cdt-" + options.parent + '-' + options.product_id + " .cdtHrs div:last-child").html(timerValues.hours.caption);
				}
				$("#cdt-" + options.parent + '-' + options.product_id + " .cdtMins div:first-child span").html(timerValues.minutes.value);
				if(options.caption.status) {
					$("#cdt-" + options.parent + '-' + options.product_id + " .cdtMins div:last-child").html(timerValues.minutes.caption);
				}
				if(options.seconds) {
					$("#cdt-" + options.parent + '-' + options.product_id + " .cdtSecs div:first-child span").html(timerValues.seconds.value);
					if(options.caption.status) {
						$("#cdt-" + options.parent + '-' + options.product_id + " .cdtSecs div:last-child").html(timerValues.seconds.caption);
					}
				}
			}
		}
		
		function setTimerHTML() {
			var html = '';
			if(options.title.status) { 
				html += '<div class="cdtTitle">' + options.title.text + '</div>';
			}
			html += '<div class="cdtTablet" id="cdt-' + options.parent + '-' + options.product_id + '">';
				if(options.days) {
					html += '<div class="cdtDays">';
						html += '<div><span></span></div>';
						if(options.caption.status) {
							html += '<div></div>';
						}
					html += '</div>';
					if(options.separator) {
						html += '<div class="cdtSeparator1">:</div>';
					}
				}
				html += '<div class="cdtHrs">';
					html += '<div><span></span></div>';
					if(options.caption.status) {
						html += '<div></div>';
					}
				html += '</div>';
				if(options.separator) {
					html += '<div class="cdtSeparator2">:</div>';
				}
				html += '<div class="cdtMins">';
					html += '<div><span></span></div>';
					if(options.caption.status) {
						html += '<div></div>';
					}
				html += '</div>';
				if(options.seconds) {
					if(options.separator) {
						html += '<div class="cdtSeparator3">:</div>';
					}
					html += '<div class="cdtSecs">';
						html += '<div><span></span></div>';
						if(options.caption.status) {
							html += '<div></div>';
						}
					html += '</div>';
				}
			html += '</div>';
			$("#cdtBlock-" + options.parent + '-' + options.product_id).html(html);
			$("#cdtBlock-" + options.parent + '-' + options.product_id).TSCountDownTimerCustomize(cdtCustomize);
		}
		
		var started = false;
		var interval = 50;
		setInterval(function() {
			if(!started) {
				setTimerHTML();
				started = true;
				interval = 1000;
			}
			setTimerValues();
		}, interval);
	},
	
	TSCountDownTimerCustomize: function(options) {
		var options = $.extend({
			title_size:			12,
			title_style:		{ b:0, i:0, u:0 },
			title_color:		'#616161',
			tablet_bgexists:	0,
			tablet_bgcolor:		'#cccccc',
			tablet_bggrad:		0,
			tablet_brdexists:	0,
			tablet_brdcolor:	'#919191',
			tablet_brdradius:	3,
			tablet_size:		28,
			tablet_style:		{ b:0, i:0, u:0 },
			tablet_color:		'#616161',
			separator_color:	'#616161',
		}, options);

		if(options['title_size']!=0) {
			this.find(".cdtTitle").css("font-size", options['title_size']+"px");
		}
		if(options['title_style']!=0) {
			if(options['title_style']['b']!=0 && options['title_style']['b']!=undefined) {
				this.find(".cdtTitle").css("font-weight", "bold");
			}
			if(options['title_style']['i']!=0 && options['title_style']['i']!=undefined) {
				this.find(".cdtTitle").css("font-style", "italic");
			}
			if(options['title_style']['u']!=0 && options['title_style']['u']!=undefined) {
				this.find(".cdtTitle").css("text-decoration", "underline");
			}
		}
		if(options['title_color']!=0) {
			this.find(".cdtTitle").css("color", options['title_color']);
		}
		
		if(options['tablet_bgexists']==0) {
			this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").css("background", "none");
		} else {
			if(options['tablet_bgcolor']!=0) {
				this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").css("backgroundColor", options['tablet_bgcolor']);
			}
			if(options['tablet_bggrad']==0) {
				this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").css("backgroundImage", "none");
			} else {
				this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").css("backgroundImage", "url('/catalog/view/javascript/tramplin-studio/CountdownTimer/image/gradient.png')");
			}
		}

		if(options['tablet_brdexists']==0) {
			this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").css("borderWidths", "0");
		} else {
			if(options['tablet_brdcolor']!=0) {
				this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").css("border", "1px solid "+options['tablet_brdcolor']);
			}
			if(options['tablet_brdradius']!=0) {
				this.find(".cdtDays div:first-child, .cdtHrs div:first-child, .cdtMins div:first-child, .cdtSecs div:first-child").corner(options['tablet_brdradius']+"px");
			}
		}

		if(options['tablet_size']!=0) {
			this.find(".cdtDays div:first-child span, .cdtHrs div:first-child span, .cdtMins div:first-child span, .cdtSecs div:first-child span").css("font-size", options['tablet_size']+"px");
			this.find(".cdtSeparator1, .cdtSeparator2, .cdtSeparator3").css("font-size", options['tablet_size']+"px");
		}

		if(options['tablet_style']!=0) {
			if(options['tablet_style']['b']!=0 && options['tablet_style']['b']!=undefined) {
				this.find(".cdtDays div:first-child span, .cdtHrs div:first-child span, .cdtMins div:first-child span, .cdtSecs div:first-child span").css("font-weight", "bold");
				this.find(".cdtSeparator1, .cdtSeparator2, .cdtSeparator3").css("font-weight", "bold");
			}
			if(options['tablet_style']['i']!=0 && options['tablet_style']['i']!=undefined) {
				this.find(".cdtDays div:first-child span, .cdtHrs div:first-child span, .cdtMins div:first-child span, .cdtSecs div:first-child span").css("font-style", "italic");
			}
			if(options['tablet_style']['u']!=0 && options['tablet_style']['u']!=undefined) {
				this.find(".cdtDays div:first-child span, .cdtHrs div:first-child span, .cdtMins div:first-child span, .cdtSecs div:first-child span").css("text-decoration", "underline");
			}
		}
		
		if(options['tablet_color']!=0) {
			this.find(".cdtDays div:first-child span, .cdtHrs div:first-child span, .cdtMins div:first-child span, .cdtSecs div:first-child span").css("color", options['tablet_color']);
		}
		
		if(options['separator_color']!=0) {
			this.find(".cdtSeparator1, .cdtSeparator2, .cdtSeparator3").css("color", options['separator_color']);
			this.find(".cdtDays div:last-child, .cdtHrs div:last-child, .cdtMins div:last-child, .cdtSecs div:last-child").css("color", options['separator_color']);
		}
	}
});
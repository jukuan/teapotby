<?php

function generateProductCategoryPrice($price, $price_weight, $product_weight, $weightTrigger ){
    if($weightTrigger != 'weight') return $price;
    if($price_weight > 0 && $product_weight > 0 && $weightTrigger == 'weight') {
        return $price_weight;
    }else{
        return $price;
    }
}
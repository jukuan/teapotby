<?php

function price($price, string $currency = 'Руб'): string
{
    if (!is_numeric($price)) {
        $price = (float) $price;
    }

    return sprintf(
        '<span class="prc-wr"><span class="prc-val">%s</span><span class="prc-cur">%s</span></span>',
        $price,
        $currency
    );
}

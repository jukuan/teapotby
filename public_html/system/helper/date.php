<?php

const MONTHS_RU = [
    'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
];

/**
 * Locale date in ru for template blogger.twig
 *
 * @param string $date
 * @return string
 */
function formatRuDate(string $date): string
{
    $timeStamp = strtotime($date);
    $month = date('n', $timeStamp) - 1;

    return date('d, ' . mb_strimwidth(MONTHS_RU[$month], 0, 3, '') . ' Y', $timeStamp);
}

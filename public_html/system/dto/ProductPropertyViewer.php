<?php

declare(strict_types=1);

class ProductPropertyViewer extends  PriceViewer
{
    private const SIMPLE_FIELDS = [
        'sku' => 'Артикул',
        'model' => 'Модель',
        'location' => 'Страна',
        //'date_available' => 'Дата добавления',
    ];

    /**
     * @var ProductProperty[]
     */
    private array $list = [];

    public bool $isBox = false;
    public bool $isTea = false;
    public bool $isCoffee = false;
    public bool $isCacao = false;
    public bool $isUtensil = false;
    public bool $isWeight = false;

    /**
     * @param string[] $fields
     */
    public function __construct(array $fields)
    {
        parent::__construct($fields);

        $this->decodeProductTypeCode($fields['jan'] ?? '');

        $translations = self::SIMPLE_FIELDS;

        if ($this->isTea) {
            $translations['model'] = 'Произрастание';
        }

        foreach ($translations as $key => $title) {
            $value = $fields[$key] ?? '';

            if (mb_strlen($key) > 0) {
                $this->list[] = new ProductProperty($title, $value);
            }
        }
    }

    private function decodeProductTypeCode(string $encoded)
    {
        $parts = str_split($encoded);
        $parts = array_pad($parts, 6, '0');

        $this->isBox = 'B' === $parts[0];
        $this->isTea = 'T' === $parts[1];
        $this->isCoffee = 'C' === $parts[2];
        $this->isCacao = 'A' === $parts[3];
        $this->isUtensil = 'U' === $parts[4];
        $this->isWeight = 'W' === $parts[5];
    }

    /**
     * @return ProductProperty[]
     */
    public function getList(): array
    {
        return $this->list;
    }
}

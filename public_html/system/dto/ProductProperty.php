<?php

declare(strict_types=1);

class ProductProperty
{
    private string $title;
    private string $value;

    public function __construct(string $title, $value)
    {
        $this->title = $title;

        $value = (string) $value;
        $this->value = trim($value);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}

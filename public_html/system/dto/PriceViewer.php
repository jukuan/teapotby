<?php

declare(strict_types=1);

class PriceViewer
{
    private const MIN_PRICE = 0.5;

    protected float $priceBox;
    protected float $priceRetail;
    protected float $priceCorp;

    private static function initialisePrice(string $value): float
    {
        $value = trim($value);

        return (float)$value;
    }

    public function __construct($fields)
    {
        $fields = (array)$fields;
        $this->priceBox = self::initialisePrice($fields['width'] ?? '');
        $this->priceRetail = self::initialisePrice($fields['length'] ?? '');
        $this->priceCorp = self::initialisePrice($fields['height'] ?? '');
    }

    public function getPriceBox(): float
    {
        return $this->priceBox;
    }

    public function getPriceRetail(): float
    {
        return $this->priceRetail;
    }

    public function getPriceCorp(): float
    {
        return $this->priceCorp;
    }

    public function hasPriceCorp(): bool
    {
        return false;
        return $this->priceCorp >= self::MIN_PRICE;
    }
}
